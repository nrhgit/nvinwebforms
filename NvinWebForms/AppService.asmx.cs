﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

namespace NvinWebForms
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class AppService : System.Web.Services.WebService
    {
        #region Respond

        private void RespondEmpty(string token)
        {
            Respond(string.Empty, null, token);
        }

        private void RespondData(object data, string token)
        {
            Respond(data, null, token);
        }

        private void RespondError(string message, string token)
        {
            // TODO: 
        }

        private void RespondException(Exception exc, string token)
        {
            Respond(null, exc, token);
        }

        private void Respond(object data, Exception exc, string token)
        {
            Result result = new Result();
            result.Data = data;
            // TODO: check messages and pass only suitable message back to client
            // TODO: log messages
            result.Message = (exc == null ? string.Empty : exc.Message);
            result.Token = token;
            // TODO: check validity of the token, renew if required

            Context.Response.Write(new JavaScriptSerializer().Serialize(result));
        }

        #endregion

        #region Forms Management

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetForms(string token)
        {
            try
            {
                Token tkn = TokenAdapter.Convert(token);

                FormAdapter adapter = new FormAdapter();
                List<Form> forms = adapter.GetForms(tkn.UserId);

                RespondData(forms, token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetForm(string token, string data)
        {
            string uniqueName = data.Trim(@"""".ToCharArray());

            try
            {
                FormAccess formAccess = AccessValidator.ValidateFormAccess(token, uniqueName);

                // any valid access (view, edit, admin)
                if (formAccess.ViewAccess || formAccess.EditAccess || formAccess.AdminAccess)
                {
                    FormAdapter adapter = new FormAdapter();
                    Form form = adapter.GetForm(formAccess.UserId, uniqueName);
                    form.FormAccess = formAccess;

                    RespondData(form, token);
                }
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        public void SaveForm(string token, string data)
        {
            try
            {
                Token tkn = TokenAdapter.Convert(token);

                JavaScriptSerializer js = new JavaScriptSerializer();
                Form form = js.Deserialize<Form>(data);

                FormAdapter adapter = new FormAdapter();
                if (form.Id == -1)
                {
                    adapter.Add(form);

                    adapter.SetFormUsers(form.UniqueName, new FormAccess[] { new FormAccess() { UserId = tkn.UserId, ViewAccess = true, EditAccess = true, AdminAccess = true } });
                }
                else
                {
                    FormAccess formAccess = AccessValidator.ValidateFormAccess(token, form.UniqueName);
                    if (formAccess.AdminAccess)
                    {
                        adapter.Update(formAccess.UserId, form);
                    }
                }

                RespondData(form.UniqueName, token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        public void DeleteForm(string token, string data)
        {
            string uniqueName = data.Trim(@"""".ToCharArray());

            try
            {
                FormAccess formAccess = AccessValidator.ValidateFormAccess(token, uniqueName);

                if (formAccess.AdminAccess)
                {
                    FormAdapter adapter = new FormAdapter();
                    adapter.Remove(formAccess.UserId, uniqueName);
                }

                RespondEmpty(token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }
        
        [WebMethod]
        public void CloneForm(string token, string data)
        {
            string uniqueName = data.Trim(@"""".ToCharArray());

            Token tkn = TokenAdapter.Convert(token);

            FormAccess formAccess = AccessValidator.ValidateFormAccess(token, uniqueName);

            if (formAccess.ViewAccess || formAccess.EditAccess || formAccess.AdminAccess)
            {
                FormAdapter adapter = new FormAdapter();
                Form form = adapter.Clone(formAccess.UserId, uniqueName);

                adapter.SetFormUsers(form.UniqueName, new FormAccess[] { new FormAccess() { UserId = tkn.UserId, ViewAccess = true, EditAccess = true, AdminAccess = true } });

                Context.Response.Write(new JavaScriptSerializer().Serialize(form));
            }
        }
        
        [WebMethod]
        public void CopyForm(string token, string data)
        {
            string uniqueName = data.Trim(@"""".ToCharArray());

            Token tkn = TokenAdapter.Convert(token);

            FormAccess formAccess = AccessValidator.ValidateFormAccess(token, uniqueName);

            if (formAccess.ViewAccess || formAccess.EditAccess || formAccess.AdminAccess)
            {
                FormAdapter adapter = new FormAdapter();
                Form form = adapter.Copy(formAccess.UserId, uniqueName);

                adapter.SetFormUsers(form.UniqueName, new FormAccess[] { new FormAccess() { UserId = tkn.UserId, ViewAccess = true, EditAccess = true, AdminAccess = true } });

                Context.Response.Write(new JavaScriptSerializer().Serialize(form));
            }
        }
        //..
        [WebMethod]
        public void ExportForm(string token, string form)
        {
            try
            {
                string uniqueName = form.Trim(@"""".ToCharArray());

                FormAccess formAccess = AccessValidator.ValidateFormAccess(token, uniqueName);

                if (formAccess.ViewAccess || formAccess.EditAccess || formAccess.AdminAccess)
                {
                    FormAdapter adapter = new FormAdapter();
                    FormExportResult result = adapter.Export(formAccess.UserId, formAccess.AdminAccess, uniqueName);

                    if (File.Exists(result.ExportFile))
                    {
                        Byte[] bytes = File.ReadAllBytes(result.ExportFile);

                        HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                        HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + result.FileName + "\"");
                        HttpContext.Current.Response.BinaryWrite(bytes);
                    }
                }
            }
            catch (Exception exc)
            {
                // TODO: handle exception and pass message back to client
            }
        }
        //..
        [WebMethod]
        public void GetAttachments(string token, string data)
        {
            string uniqueName = data.Trim(@"""".ToCharArray());

            try
            {
                FormAccess formAccess = AccessValidator.ValidateFormAccess(token, uniqueName);

                if (formAccess.ViewAccess || formAccess.EditAccess || formAccess.AdminAccess)
                {
                    FormAdapter adapter = new FormAdapter();
                    GetAttachmentsData attachementsData = adapter.GetAttachments(uniqueName);

                    RespondData(attachementsData, token);
                }
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }
        //..
        [WebMethod]
        public void UploadFormAttachment(string token, string data)
        {
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                UploadAttachmentInput input = js.Deserialize<UploadAttachmentInput>(data);

                FormAccess formAccess = AccessValidator.ValidateFormAccess(token, input.FormId);

                if (formAccess.EditAccess || formAccess.AdminAccess)
                {
                    FormAdapter adapter = new FormAdapter();
                    adapter.UploadFormAttachment(input.FormId, input.FileName, input.Content);
                }

                RespondEmpty(token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }
        //..
        [WebMethod]
        public void DownloadAttachment(string token, string form, string name)
        {
            string path = @"C:\FileStore\Attachments\" + form + @"\" + name;
            if (File.Exists(path))
            {
                string content = File.ReadAllText(path);
                Byte[] bytes = Convert.FromBase64String(content);

                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name + "\"");
                HttpContext.Current.Response.BinaryWrite(bytes);
            }
        }
        //..
        [WebMethod]
        public void RemoveFormAttachment(string token, string data)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            RemoveAttachmentInput input = js.Deserialize<RemoveAttachmentInput>(data);

            FormAccess formAccess = AccessValidator.ValidateFormAccess(token, input.FormId);

            if (formAccess.EditAccess || formAccess.AdminAccess)
            {
                FormAdapter adapter = new FormAdapter();
                adapter.RemoveFormAttachment(input.FormId, input.FileName);
            }
        }
        //..
        [WebMethod]
        public void UploadFormTemplate(string token, string data)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            UploadAttachmentInput input = js.Deserialize<UploadAttachmentInput>(data);

            FormAccess formAccess = AccessValidator.ValidateFormAccess(token, input.FormId);

            if (formAccess.EditAccess || formAccess.AdminAccess)
            {
                FormAdapter adapter = new FormAdapter();
                adapter.UploadFormTemplate(input.FormId, input.FileName, input.Content);
            }
        }
        //..
        [WebMethod]
        public void RemoveFormTemplate(string token, string data)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            RemoveAttachmentInput input = js.Deserialize<RemoveAttachmentInput>(data);

            FormAccess formAccess = AccessValidator.ValidateFormAccess(token, input.FormId);

            if (formAccess.EditAccess || formAccess.AdminAccess)
            {
                FormAdapter adapter = new FormAdapter();
                adapter.RemoveFormTemplate(input.FormId, input.FileName);
            }
        }
        //..
        [WebMethod]
        public void UploadRowTemplate(string token, string data)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            UploadAttachmentInput input = js.Deserialize<UploadAttachmentInput>(data);

            FormAccess formAccess = AccessValidator.ValidateFormAccess(token, input.FormId);

            if (formAccess.EditAccess || formAccess.AdminAccess)
            {
                FormAdapter adapter = new FormAdapter();
                adapter.UploadRowTemplate(input.FormId, input.FileName, input.Content);
            }
        }
        //..
        [WebMethod]
        public void RemoveRowTemplate(string token, string data)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            RemoveAttachmentInput input = js.Deserialize<RemoveAttachmentInput>(data);

            FormAccess formAccess = AccessValidator.ValidateFormAccess(token, input.FormId);

            if (formAccess.EditAccess || formAccess.AdminAccess)
            {
                FormAdapter adapter = new FormAdapter();
                adapter.RemoveRowTemplate(input.FormId, input.FileName);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetFormData(string token, string data)
        {
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                GetFormDataInput input = js.Deserialize<GetFormDataInput>(data);

                FormAccess formAccess = AccessValidator.ValidateFormAccess(token, input.FormId);

                if (formAccess.ViewAccess || formAccess.EditAccess || formAccess.AdminAccess)
                {
                    FormAdapter adapter = new FormAdapter();
                    GetFormDataResult result = adapter.GetForm(formAccess.UserId, formAccess.AdminAccess, input.FormId, input.PageIndex, input.PageSize, input.Filter);

                    result.FormAccess = formAccess;

                    RespondData(result, token);
                }
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        public void DownloadData(string token, string data)
        {
            string uniqueName = data.Trim(@"""".ToCharArray());

            FormAccess formAccess = AccessValidator.ValidateFormAccess(token, uniqueName);

            if (formAccess.ViewAccess || formAccess.EditAccess || formAccess.AdminAccess)
            {
                FormAdapter adapter = new FormAdapter();
                string csv = adapter.CSVExport(formAccess.UserId, uniqueName);

                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=data.csv"); // TODO: Use form name
                HttpContext.Current.Response.Write(csv);
            }
        }

        [WebMethod]
        public void DownloadFormFile(string token, string form, string file, string name)
        {
            string path = @"C:\FileStore\FormFiles\" + form + @"\" + file;
            if (File.Exists(path))
            {
                string content = File.ReadAllText(path);
                Byte[] bytes = Convert.FromBase64String(content);

                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=\"" + name +"\""); // TODO: Use form name
                HttpContext.Current.Response.BinaryWrite(bytes);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetFormRowData(string token, string data)
        {
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                FormRowDataInput input = js.Deserialize<FormRowDataInput>(data);

                FormAccess formAccess = AccessValidator.ValidateFormAccess(token, input.FormId);

                if (formAccess.EditAccess || formAccess.AdminAccess)
                {
                    FormAdapter adapter = new FormAdapter();
                    
                    GetFormRowDataResult result = new GetFormRowDataResult();
                    result.Form = adapter.GetForm(formAccess.UserId, formAccess.AdminAccess, input.FormId, input.RowId);
                    result.FormAccess = formAccess;
                    result.IsRowCommitted = adapter.IsRowCommitted(input.FormId, input.RowId);

                    RespondData(result, token);
                }
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void SaveFormData(string token, string data)
        {
            SaveOrCommitFormData(token, data, false);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void CommitFormData(string token, string data)
        {
            SaveOrCommitFormData(token, data, true);
        }

        private void SaveOrCommitFormData(string token, string data, bool isCommitted)
        {
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                FormDataInput input = js.Deserialize<FormDataInput>(data);

                FormAccess formAccess = AccessValidator.ValidateFormAccess(token, input.FormId);

                if (formAccess.EditAccess || formAccess.AdminAccess)
                {
                    FormAdapter adapter = new FormAdapter();
                    if (input.RowId == "none")
                    {
                        string rowId = adapter.AddFormData(formAccess.UserId, isCommitted, input.FormId, input.ColValues);
                        RespondData(rowId.Trim("'".ToCharArray()), token);
                    }
                    else
                    {
                        adapter.UpdateFormData(formAccess.UserId, isCommitted, input.FormId, input.RowId, input.ColValues);
                        RespondData(input.RowId, token);
                    }
                }
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        public void DeleteFormRowData(string token, string data)
        {
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                FormRowDataInput input = js.Deserialize<FormRowDataInput>(data);

                FormAccess formAccess = AccessValidator.ValidateFormAccess(token, input.FormId);

                if (formAccess.EditAccess || formAccess.AdminAccess)
                {
                    FormAdapter adapter = new FormAdapter();
                    adapter.DeleteFormData(input.FormId, input.RowId);
                }

                RespondEmpty(token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        public void CopyFormRowData(string token, string data)
        {
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                FormRowDataInput input = js.Deserialize<FormRowDataInput>(data);

                FormAccess formAccess = AccessValidator.ValidateFormAccess(token, input.FormId);

                if (formAccess.EditAccess || formAccess.AdminAccess)
                {
                    FormAdapter adapter = new FormAdapter();
                    adapter.CopyFormData(input.FormId, input.RowId);
                }

                RespondEmpty(token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetFormUsers(string token, string data)
        {
            try
            {
                string uniqueName = data.Trim(@"""".ToCharArray());

                FormAccess formAccess = AccessValidator.ValidateFormAccess(token, uniqueName);

                if (formAccess.AdminAccess)
                {
                    FormAdapter adapter = new FormAdapter();
                    List<FormAccess> formUsers = adapter.GetFormUsers(uniqueName);

                    RespondData(formUsers ,token);
                }
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void SetFormUsers(string token, string data)
        {
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                SetFormUsersInput input = js.Deserialize<SetFormUsersInput>(data);

                FormAccess formAccess = AccessValidator.ValidateFormAccess(token, input.UniqueName);

                if (formAccess.AdminAccess)
                {
                    FormAdapter adapter = new FormAdapter();
                    adapter.SetFormUsers(input.UniqueName, input.FormUsers);
                }

                RespondEmpty(token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetFormTemplates(string token)
        {
            try
            {
                FormAdapter adapter = new FormAdapter();
                List<FormTemplate> formTemplates = adapter.GetFormTemplates();

                RespondData(formTemplates, token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ImportFormTemplate(string token, string data)
        {
            try
            {
                Token tkn = TokenAdapter.Convert(token);

                string uniqueName = data.Trim(@"""".ToCharArray());

                FormAdapter adapter = new FormAdapter();
                Form form = adapter.Clone(tkn.UserId, uniqueName);
                
                adapter.SetFormUsers(form.UniqueName, new FormAccess[] { new FormAccess() { UserId = tkn.UserId, ViewAccess = true, EditAccess = true, AdminAccess = true } });

                RespondData(form.UniqueName, token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        #endregion
        
        #region User Management

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetUsers(string token)
        {
            try
            {
                UserAdapter adapter = new UserAdapter();
                List<User> users = adapter.GetUsers();

                RespondData(users, token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetUser(string token, string data)
        {
            try
            {
                int userId = Int32.Parse(data.Trim(@"""".ToCharArray()));

                UserAdapter adapter = new UserAdapter();
                User user = adapter.GetUser(userId);

                RespondData(user, token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void SaveUser(string token, string data)
        {
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                User user = js.Deserialize<User>(data);

                UserAdapter adapter = new UserAdapter();
                if (user.Id == -1)
                {
                    adapter.Add(user);
                }
                else
                {
                    adapter.Update(user);
                }

                RespondEmpty(token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        public void DeleteUser(string token, string data)
        {
            try
            {
                int userId = Int32.Parse(data.Trim(@"""".ToCharArray()));

                UserAdapter adapter = new UserAdapter();
                adapter.Remove(userId);

                RespondEmpty(token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetGroups(string token)
        {
            try
            {
                GroupAdapter adapter = new GroupAdapter();
                List<Group> groups = adapter.GetGroups();

                RespondData(groups, token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetGroupsAndUsers(string token)
        {
            try
            {
                GroupAdapter adapter = new GroupAdapter();
                List<Group> groups = adapter.GetGroupsAndUsers();

                RespondData(groups, token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetGroup(string token, string data)
        {
            try
            {
                int groupId = Int32.Parse(data.Trim(@"""".ToCharArray()));

                GroupAdapter adapter = new GroupAdapter();
                Group group = adapter.GetGroup(groupId);

                RespondData(group, token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void SaveGroup(string token, string data)
        {
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                Group group = js.Deserialize<Group>(data);

                GroupAdapter adapter = new GroupAdapter();
                if (group.Id == -1)
                {
                    adapter.Add(group);
                }
                else
                {
                    adapter.Update(group);
                }
                RespondEmpty(token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        public void DeleteGroup(string token, string data)
        {
            try
            {
                int groupId = Int32.Parse(data.Trim(@"""".ToCharArray()));

                GroupAdapter adapter = new GroupAdapter();
                adapter.Remove(groupId);

                RespondEmpty(token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetGroupUsers(string token, string data)
        {
            try
            {
                int groupId = Int32.Parse(data.Trim(@"""".ToCharArray()));

                GroupAdapter adapter = new GroupAdapter();
                List<int> users = adapter.GetGroupUsers(groupId);

                RespondData(users, token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void AssignUsers(string token, string data)
        {
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                AssignGroupUsersInput input = js.Deserialize<AssignGroupUsersInput>(data);

                GroupAdapter adapter = new GroupAdapter();
                adapter.AssignUsers(input.GroupId, input.AssignedUsers);

                RespondEmpty(token);
            }
            catch (Exception exc)
            {
                RespondException(exc, token);
            }
        }

        #endregion

        #region Authenticate

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Authenticate(string data)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            AuthenticateInput input = js.Deserialize<AuthenticateInput>(data);

            string token = Authenticator.Autheticate(input.UserName, input.Pwd);

            Result result = new Result() { Token = token };

            Context.Response.Write(new JavaScriptSerializer().Serialize(result));
        }

        #endregion
    }
}