﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    [Serializable]
    internal class Token
    {
        public DateTime CreateDateTime { get; set; }

        public int Validity { get; set; }

        public int UserId { get; set; }

        //public int CompanyId { get; set; }

        public string RandomText { get; set; }

        public Token()
        {
            this.CreateDateTime = DateTime.Now;
            this.Validity = 30; // mins
            this.RandomText = Guid.NewGuid().ToString();
        }

        public Token(int userId)
            : this()
        {
            this.UserId = userId;
        }

        public override string ToString()
        {
            return CreateDateTime.ToString("yyyy-MMM-dd HH:mm:ss") + "|" + Validity + "|" + UserId + "|" + RandomText;
        }
    }
}