﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class Form
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public string UniqueName { get; set; }

        public bool UserSpecific { get; set; }

        public bool AllowCommit { get; set; }

        public List<FormColumn> Columns { get; set; }

        public List<List<string>> Rows { get; set; }

        public FormAccess FormAccess { get; set; }

        public Form()
        {
            this.Columns = new List<FormColumn>();

            this.Rows = new List<List<string>>();
        }
    }
}