﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Web;

namespace NvinWebForms
{
    internal class ExcelAdapter
    {
        private const string dir = @"C:\FileStore\Temp\";
        private string connectionString;
        private bool isXlsx;
        private bool sheetExists = false;
        private bool dataExists = false;
        private bool columnsMatch = true;

        private Form form;

        public string ExportFileName { get; set; }

        public string FileName
        {
            get
            {
                string filename = string.Empty;
                if (this.form != null && String.IsNullOrEmpty(ExportFileName) == false)
                {
                    filename = this.form.Description + Path.GetExtension(ExportFileName);
                }
                return filename;
            }
        }

        public ExcelAdapter(int userId, bool isAdmin, string uniqueName)
        {
            FormAdapter formAdapter = new FormAdapter();
            GetFormDataResult result = formAdapter.GetForm(userId, isAdmin, uniqueName, 0, Int32.MaxValue, string.Empty);
            if (result == null) throw new Exception("Unable to get data from form with unique ID: " + uniqueName);

            this.form = result.Form;

            string template = GetTemplate(uniqueName);

            this.ExportFileName = dir + Guid.NewGuid().ToString().Replace("-", "");
            this.ExportFileName += (String.IsNullOrEmpty(template) ? ".xlsx" : Path.GetExtension(template));

            if (File.Exists(template))
            {
                File.Copy(template, this.ExportFileName);
            }
            else
            {
                File.Copy(HttpContext.Current.Server.MapPath("templates/blank.xlsx"), this.ExportFileName);
            }

            this.isXlsx = this.ExportFileName.EndsWith("xlsx");
            this.connectionString = (isXlsx ? "Provider=Microsoft.ACE.OLEDB.12.0;Extended Properties=Excel 12.0 XML;" : "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=Excel 8.0;") + ("Data Source=" + this.ExportFileName + ";");
        }

        public void Export()
        {
            CheckDataSheet();

            if (dataExists) throw new Exception("Export failed as Data sheet in template contains data.");

            if (columnsMatch == false) throw new Exception("Export failed as columns in Data sheet of template file does not match with form columns");

            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                conn.Open();

                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = conn;

                List<string> columns = new List<string>();
                foreach (FormColumn column in form.Columns)
                {
                    columns.Add("[" + column.Description + "]" + " text");
                }

                if (sheetExists == false)
                {
                    cmd.CommandText = String.Format("create table Data({0})", String.Join(",", columns.ToArray()));
                    cmd.ExecuteNonQuery();
                }

                foreach (List<string> row in form.Rows)
                {
                    List<string> cols = new List<string>();
                    List<OleDbParameter> parameters = new List<OleDbParameter>();
                    for (int i = 1; i < row.Count; i++)
                    {
                        string val = row[i];
                        parameters.Add(new OleDbParameter() { ParameterName = "@col" + i, OleDbType = OleDbType.VarChar, Direction = ParameterDirection.Input, Value = val });

                        cols.Add("@col" + i);
                    }

                    cmd.CommandText = String.Format("INSERT INTO Data VALUES({0})", String.Join(",", cols.ToArray()));
                    cmd.Parameters.AddRange(parameters.ToArray());
                    cmd.ExecuteNonQuery();
                }

                conn.Close();
            }
        }

        private void CheckDataSheet()
        {
            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                conn.Open();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = conn;

                DataTable dtSheet = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                foreach (DataRow dr in dtSheet.Rows)
                {
                    string sheetName = dr["TABLE_NAME"].ToString();

                    if (!sheetName.EndsWith("$")) continue;

                    if (sheetName == "Data$")
                    {
                        sheetExists = true;

                        DataTable dt = new DataTable();

                        cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                        OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                        da.Fill(dt);

                        columnsMatch = columnsMatch && (form.Columns.Count == dt.Columns.Count);

                        if (columnsMatch)
                        {
                            for (int i = 0; i < dt.Columns.Count; i++)
                            {
                                DataColumn dtColumn = dt.Columns[i];
                                FormColumn column = form.Columns[i];

                                columnsMatch = columnsMatch && (dtColumn.ColumnName == "[" + column.Description + "]");

                                if (columnsMatch == false) break;
                            }
                        }

                        if (dt.Rows.Count > 0)
                        {
                            dataExists = true;
                        }
                    }
                }

                conn.Close();
            }
        }

        private string GetTemplate(string uniqueName)
        {
            string formTemplateDir = @"C:\FileStore\FormTemplate\" + uniqueName;
            string[] files = Directory.GetFiles(formTemplateDir);

            if (files.Length > 0)
            {
                return files[0];
            }

            return string.Empty;
        }
    }
}