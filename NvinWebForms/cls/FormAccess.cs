﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    public class FormAccess
    {
        public int UserId { get; set; }

        public bool ViewAccess { get; set; }

        public bool EditAccess { get; set; }

        public bool AdminAccess { get; set; }

        public FormAccess()
        {
            this.ViewAccess = false;
            this.EditAccess = false;
            this.AdminAccess = false;
        }
    }
}