﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace NvinWebForms
{
    internal class AccessValidator
    {
        internal static FormAccess ValidateFormAccess(string tokenText, string uniqueName)
        {
            FormAccess access = new FormAccess();

            Token token = TokenAdapter.Convert(tokenText);
            if (token != null)
            {
                access = ValidateFormAccess(token.UserId, uniqueName);
            }

            return access;
        }

        internal static FormAccess ValidateFormAccess(int userId, string uniqueName)
        {
            FormAccess access = new FormAccess();

            string sql = "select * from FormAccess where UniqueName = @uniqueName and UserId = @userId";

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@uniqueName", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = uniqueName });
            parameters.Add(new SqlParameter() { ParameterName = "@userId", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = userId });

            DataTable dt = DatabaseAdapter.Instance.GetData(sql, parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];

                access.UserId = userId;
                access.ViewAccess = (bool)row["ViewAccess"];
                access.EditAccess = (bool)row["EditAccess"];
                access.AdminAccess = (bool)row["AdminAccess"];
            }

            return access;
        }
    }
}