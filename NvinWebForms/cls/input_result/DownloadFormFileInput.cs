﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class DownloadFormFileInput
    {
        public string FormId { get; set; }
        public string FileId { get; set; }
        public string FileName { get; set; }
    }
}