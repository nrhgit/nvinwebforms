﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class UploadAttachmentInput
    {
        public string FormId { get; set; }

        public string FileName { get; set; }

        public string Content { get; set; }
    }
}