﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class FormRowDataInput
    {
        public string FormId { get; set; }
        public string RowId { get; set; }
    }
}