﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class FormExportResult
    {
        public string FileName { get; set; }

        public string ExportFile { get; set; }
    }
}