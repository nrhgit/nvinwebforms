﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class GetFormDataResult
    {
        public int Pages { get; set; }

        public Form Form { get; set; }

        public FormAccess FormAccess { get; set; }
    }
}