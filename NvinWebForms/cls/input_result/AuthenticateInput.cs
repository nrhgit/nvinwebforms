﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class AuthenticateInput
    {
        public string UserName { get; set; }

        public string Pwd { get; set; }
    }
}