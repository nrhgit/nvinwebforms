﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class FormDataInput
    {
        public string FormId { get; set; }
        public string RowId { get; set; }
        public List<ColumnValueInput> ColValues { get; set; }

        public FormDataInput()
        {
            ColValues = new List<ColumnValueInput>();
        }
    }
}