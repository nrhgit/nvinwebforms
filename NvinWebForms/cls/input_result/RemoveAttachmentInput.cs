﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class RemoveAttachmentInput
    {
        public string FormId { get; set; }

        public string FileName { get; set; }
    }
}