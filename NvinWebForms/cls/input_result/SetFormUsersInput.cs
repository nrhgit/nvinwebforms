﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class SetFormUsersInput
    {
        public string UniqueName { get; set; }

        public FormAccess[] FormUsers { get; set; }
    }
}