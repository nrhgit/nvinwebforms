﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class AssignGroupUsersInput
    {
        public int GroupId { get; set; }

        public int[] AssignedUsers { get; set; }
    }
}