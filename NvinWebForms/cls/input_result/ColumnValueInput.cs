﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    public class ColumnValueInput
    {
        public string ColId { get; set; }
        public string ColValue { get; set; }
    }
}