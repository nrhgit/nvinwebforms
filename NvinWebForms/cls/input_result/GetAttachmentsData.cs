﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class GetAttachmentsData
    {
        public List<string> FormAttachments { get; set; }

        public string FormTemplate { get; set; }

        public string RowTemplate { get; set; }

        public GetAttachmentsData()
        {
            this.FormAttachments = new List<string>();
        }
    }
}