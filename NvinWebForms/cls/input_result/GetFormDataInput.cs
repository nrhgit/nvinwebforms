﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class GetFormDataInput
    {
        public string FormId { get; set; }

        public string Filter { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }
    }
}