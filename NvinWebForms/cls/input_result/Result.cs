﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class Result
    {
        public object Data { get; set; }

        public string Token { get; set; } // required to send back renewed token

        public string Message { get; set; }

        public Result()
        {
            this.Data = null;
            this.Token = string.Empty;
            this.Message = string.Empty;
        }
    }
}