﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class GetFormRowDataResult
    {
        public Form Form { get; set; }

        public FormAccess FormAccess { get; set; }

        public bool IsRowCommitted { get; set; }
    }
}