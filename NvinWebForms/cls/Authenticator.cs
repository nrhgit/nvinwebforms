﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace NvinWebForms
{
    internal class Authenticator
    {
        public static string Autheticate(string username, string password)
        {
            string tokenText = string.Empty;

            string sql = "select Id from Users where Email = @email and Pwd = @pwd";

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@email", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = username });
            parameters.Add(new SqlParameter() { ParameterName = "@pwd", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = password });

            DataTable dt = DatabaseAdapter.Instance.GetData(sql, parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                int userId = (int)dt.Rows[0]["Id"];

                Token token = new Token(userId);
                tokenText = TokenAdapter.Convert(token);
            }

            return tokenText;
        }
    }
}