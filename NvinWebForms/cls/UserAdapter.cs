﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace NvinWebForms
{
    internal class UserAdapter
    {
        private DatabaseAdapter dbAdapter = DatabaseAdapter.Instance;

        public List<User> GetUsers()
        {
            List<User> users = new List<User>();

            string sql = "select * from Users";
            DataTable dt = dbAdapter.GetData(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    User user = new User();
                    user.Id = (int)row["Id"];
                    user.FirstName = (string)row["FirstName"];
                    user.LastName = (string)row["LastName"];
                    user.Email = (string)row["Email"];

                    users.Add(user);
                }
            }

            return users;
        }

        public User GetUser(int id)
        {
            string sql = "select * from Users where Id = " + id;
            DataTable dt = dbAdapter.GetData(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                User user = new User();
                user.Id = (int)dt.Rows[0]["Id"];
                user.FirstName = (string)dt.Rows[0]["FirstName"];
                user.LastName = (string)dt.Rows[0]["LastName"];
                user.Email = (string)dt.Rows[0]["Email"];

                return user;
            }

            return null;
        }

        public void Add(User user)
        {
            string rowId = Guid.NewGuid().ToString().Replace("-", "");

            string sql = "insert into Users values(@fname, @lname, @email, @pwd, @rowId)";
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@fname", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = user.FirstName });
            parameters.Add(new SqlParameter() { ParameterName = "@lname", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = user.LastName });
            parameters.Add(new SqlParameter() { ParameterName = "@email", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = user.Email });
            parameters.Add(new SqlParameter() { ParameterName = "@pwd", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = (user.Password == null ? "" : user.Password) });
            parameters.Add(new SqlParameter() { ParameterName = "@rowId", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = rowId });

            dbAdapter.ExecuteSql(sql, parameters);

            sql = String.Format("select * from Users where RowId='{0}'", rowId);
            DataTable dt = dbAdapter.GetData(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                user.Id = (int)dt.Rows[0]["Id"];
            }
        }

        public void Update(User user)
        {
            string sql = "update Users set FirstName=@fname, LastName=@lname, Email=@email, Pwd=@pwd where Id=@id";
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@id", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = user.Id });
            parameters.Add(new SqlParameter() { ParameterName = "@fname", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = user.FirstName });
            parameters.Add(new SqlParameter() { ParameterName = "@lname", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = user.LastName });
            parameters.Add(new SqlParameter() { ParameterName = "@email", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = user.Email });
            parameters.Add(new SqlParameter() { ParameterName = "@pwd", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = (user.Password == null ? "" : user.Password) });

            dbAdapter.ExecuteSql(sql, parameters);
        }

        public void Remove(int id)
        {
            string sql = "delete from Users where Id=@id";
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@id", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = id });

            dbAdapter.ExecuteSql(sql, parameters);
        }
    }
}