﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace NvinWebForms
{
    internal class GroupAdapter
    {
        private DatabaseAdapter dbAdapter = DatabaseAdapter.Instance;

        public List<Group> GetGroups()
        {
            List<Group> groups = new List<Group>();

            string sql = "select * from Groups";
            DataTable dt = dbAdapter.GetData(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Group group = new Group();
                    group.Id = (int)row["Id"];
                    group.Name = (string)row["Name"];

                    groups.Add(group);
                }
            }

            return groups;
        }

        public List<Group> GetGroupsAndUsers()
        {
            List<Group> groups = GetGroups();
            foreach (Group group in groups)
            {
                string sql = "select UserId from GroupUserMapping where GroupId = " + group.Id;
                DataTable dt = dbAdapter.GetData(sql);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        group.Users.Add((int)row["UserId"]);
                    }
                }

            }

            return groups;
        }

        public Group GetGroup(int id)
        {
            string sql = "select * from Groups where Id = " + id;
            DataTable dt = dbAdapter.GetData(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                Group group = new Group();
                group.Id = (int)dt.Rows[0]["Id"];
                group.Name = (string)dt.Rows[0]["Name"];

                return group;
            }

            return null;
        }

        public void Add(Group group)
        {
            string rowId = Guid.NewGuid().ToString().Replace("-", "");

            string sql = "insert into Groups values(@name, @rowId)";
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@name", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = group.Name });
            parameters.Add(new SqlParameter() { ParameterName = "@rowId", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = rowId });

            dbAdapter.ExecuteSql(sql, parameters);

            sql = String.Format("select * from Groups where RowId='{0}'", rowId);
            DataTable dt = dbAdapter.GetData(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                group.Id = (int)dt.Rows[0]["Id"];
            }
        }

        public void Update(Group group)
        {
            string sql = "update Groups set Name=@name where Id=@id";
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@id", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = group.Id });
            parameters.Add(new SqlParameter() { ParameterName = "@name", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = group.Name });

            dbAdapter.ExecuteSql(sql, parameters);
        }

        public void Remove(int id)
        {
            string sql = "delete from Groups where Id=@id";
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@id", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = id });

            dbAdapter.ExecuteSql(sql, parameters);
        }

        public List<int> GetGroupUsers(int id)
        {
            List<int> users = new List<int>();

            string sql = @"select gm.UserId from GroupUserMapping gm
inner join Groups g on g.Id = gm.GroupId
inner join Users u on u.Id = gm.UserId where gm.GroupId = " + id;

            DataTable dt = dbAdapter.GetData(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    users.Add((int)row["UserId"]);
                }
            }

            return users;
        }

        public void AssignUsers(int groupId, int[] users)
        {
            string sql = "delete from GroupUserMapping where GroupId = " + groupId;
            dbAdapter.ExecuteSql(sql);

            foreach (int user in users)
            {
                sql = String.Format("insert into GroupUserMapping values({0}, {1})", groupId, user);
                dbAdapter.ExecuteSql(sql);
            }
        }
    }
}