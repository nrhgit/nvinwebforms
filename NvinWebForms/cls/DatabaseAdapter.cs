﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;

namespace NvinWebForms
{
    internal class DatabaseAdapter
    {
        private string connectionString = @"Server=DESKTOP-A804RRK;Database=TestX;User Id=sa;Password=naveen1!";
        private static DatabaseAdapter instance;

        private DatabaseAdapter() { }

        public static DatabaseAdapter Instance
        {
            get
            {
                if (instance == null) instance = new DatabaseAdapter();

                return instance;
            }
        }

        public DataTable GetData(string sql)
        {
            return GetData(sql, null);
        }

        public DataTable GetData(string sql, List<SqlParameter> parameters)
        {
            DataTable dt = new DataTable();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                if (parameters != null && parameters.Count > 0)
                {
                    cmd.Parameters.AddRange(parameters.ToArray());
                }

                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);

                connection.Open();

                dataAdapter.Fill(dt);
            }

            return dt;
        }

        public int ExecuteSql(string sql)
        {
            return ExecuteSql(sql, null);
        }

        public int ExecuteSql(string sql, List<SqlParameter> parameters)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand(sql, connection);
                if (parameters != null && parameters.Count > 0)
                {
                    cmd.Parameters.AddRange(parameters.ToArray());
                }

                connection.Open();

                return cmd.ExecuteNonQuery();
            }
        }
    }
}