﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    public class FormTemplate
    {
        public string FormId { get; set; }

        public string Description { get; set; }

        public string Category { get; set; }
    }
}