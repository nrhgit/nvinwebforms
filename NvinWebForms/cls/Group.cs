﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class Group
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<int> Users { get; set; }

        public Group()
        {
            this.Users = new List<int>();
        }
    }
}