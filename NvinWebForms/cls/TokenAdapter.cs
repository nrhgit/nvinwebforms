﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    internal class TokenAdapter
    {
        public static Token Convert(string tokenText)
        {
            try
            {
                tokenText = tokenText.Replace("$", "=");
                tokenText = tokenText.Replace("~", "/");
                tokenText = tokenText.Replace("^", "+");
                string decryptedTokenText = EncryptDecrypt.Decrypt(tokenText);

                if (String.IsNullOrEmpty(decryptedTokenText)) return null;

                string[] parts = decryptedTokenText.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                Token token = new Token();
                token.CreateDateTime = System.Convert.ToDateTime(parts[0]);
                token.Validity = Int32.Parse(parts[1]);
                token.UserId = Int32.Parse(parts[2]);
                token.RandomText = parts[3];

                return token;
            }
            catch { return null; }
        }

        public static string Convert(Token token)
        {
            string encryptedText = EncryptDecrypt.Encrypt(token.ToString());
            encryptedText = encryptedText.Replace("=", "$");
            encryptedText = encryptedText.Replace("/", "~");

            return encryptedText;
        }
    }
}