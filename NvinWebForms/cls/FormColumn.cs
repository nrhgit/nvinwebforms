﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    public class FormColumn
    {
        public string Id { get; set; }

        public int FormId { get; set; }

        public string Description { get; set; }

        public ColumnType ColumnType { get; set; }

        public bool ShowInSummary { get; set; }

        public string Options { get; set; }

        public string HelpText { get; set; }

        public decimal HeaderWidth { get; set; }

        public int DisplayIndex { get; set; }

        public bool AdminEditOnly { get; set; }

        public string DefaultValue { get; set; }
    }
}