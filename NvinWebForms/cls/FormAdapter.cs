﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;

namespace NvinWebForms
{
    internal class FormAdapter
    {
        private DatabaseAdapter dbAdapter = DatabaseAdapter.Instance;

        public List<Form> GetForms(int userId)
        {
            List<Form> forms = new List<Form>();

            List<string> ids = new List<string>();
            string sql = "select UniqueName from FormAccess where UserId = " + userId + " and (ViewAccess = 1 or EditAccess = 1 or AdminAccess = 1)";
            DataTable dt = dbAdapter.GetData(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string uniqueName = (string)row["UniqueName"];
                    if (ids.Contains(uniqueName) == false)
                    {
                        ids.Add("'" + uniqueName + "'");
                    }
                }
            }

            if (ids.Count > 0)
            {
                sql = String.Format("select * from Form where UniqueName in ({0})", String.Join(",", ids.ToArray()));
                dt = dbAdapter.GetData(sql);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        Form form = new Form();
                        form.Id = (int)row["Id"];
                        form.Description = (string)row["Description"];
                        form.UniqueName = (string)row["UniqueName"];
                        form.UserSpecific = (bool)row["UserSpecific"];
                        form.AllowCommit = (bool)row["AllowCommit"];
                        form.FormAccess = AccessValidator.ValidateFormAccess(userId, form.UniqueName);

                        forms.Add(form);
                    }
                }
            }

            return forms;
        }

        public Form GetForm(int userId, string uniqueName)
        {
            GetFormDataResult result = GetForm(userId, true, uniqueName, -1, Int32.MaxValue, string.Empty, string.Empty);

            return result.Form;
        }

        public GetFormDataResult GetForm(int userId, bool isAdmin, string uniqueName, int pageIndex, int pageSize, string filter)
        {
            return GetForm(userId, isAdmin, uniqueName, pageIndex, pageSize, string.Empty, filter);
        }

        public Form GetForm(int userId, bool isAdmin, string uniqueName, string rowId)
        {
            GetFormDataResult result = GetForm(userId, isAdmin, uniqueName, 0, 1, rowId, string.Empty);

            return result.Form;
        }

        public GetFormDataResult GetForm(int userId, bool isAdmin, string uniqueName, int pageIndex, int pageSize, string rowId, string filter)
        {
            string sql = "select * from Form where UniqueName = @uniqueName";
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@uniqueName", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = uniqueName });

            DataTable dt = dbAdapter.GetData(sql, parameters);

            if (dt != null && dt.Rows.Count > 0)
            {
                int pages = 0;
                Form form = new Form();
                form.Id = (int)dt.Rows[0]["Id"];
                form.Description = (string)dt.Rows[0]["Description"];
                form.UniqueName = (string)dt.Rows[0]["UniqueName"];
                form.UserSpecific = (bool)dt.Rows[0]["UserSpecific"];
                form.AllowCommit = (bool)dt.Rows[0]["AllowCommit"];

                // load columns
                sql = "select * from FormColumn where FormId = " + form.Id + " order by DisplayIndex";
                dt = dbAdapter.GetData(sql);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        FormColumn column = new FormColumn();
                        column.Id = (string)row["Id"];
                        column.FormId = form.Id;
                        column.Description = (string)row["Description"];
                        column.ColumnType = (ColumnType)(short)row["ColumnType"];
                        column.ShowInSummary = (bool)row["ShowInSummary"];
                        column.Options = (string)row["Options"];
                        column.HelpText = (string)row["HelpText"];
                        column.HeaderWidth = (decimal)row["HeaderWidth"];
                        column.DisplayIndex = (short)row["DisplayIndex"];
                        column.AdminEditOnly = (bool)row["AdminEditOnly"];
                        column.DefaultValue = (string)row["DefaultValue"];

                        form.Columns.Add(column);
                    }
                }

                List<string> columnNames = new List<string>();
                foreach (FormColumn formColumn in form.Columns)
                {
                    columnNames.Add(formColumn.Id);
                }

                if (pageIndex >= 0)
                {
                    if (isAdmin)
                    {
                        form.UserSpecific = false;
                    }

                    parameters.Clear();

                    sql = String.Format("select MyUniqueRowId, {0} from ", (String.Join(",", columnNames.ToArray()))) + form.UniqueName;
                    List<string> conditions = new List<string>();

                    List<string> userSpecificRows = new List<string>();
                    if (form.UserSpecific)
                    {
                        string specificRowsSql = "select MyUniqueRowId from " + "y" + form.UniqueName + " where UserId = " + userId;
                        DataTable specificRowsDt = dbAdapter.GetData(specificRowsSql);
                        if (specificRowsDt != null && specificRowsDt.Rows.Count > 0)
                        {
                            foreach (DataRow row in specificRowsDt.Rows)
                            {
                                userSpecificRows.Add("'" + (string)row["MyUniqueRowId"] + "'");
                            }
                        }
                    }

                    // get only row specific data
                    if (String.IsNullOrEmpty(rowId) == false)
                    {
                        if (form.UserSpecific)
                        {
                            bool rowExists = false;

                            foreach (string userSpecificRow in userSpecificRows)
                            {
                                if (userSpecificRow.Trim("'".ToCharArray()) == rowId)
                                {
                                    rowExists = true;
                                    break;
                                }
                            }

                            if (rowExists || rowId == "none")
                            {
                                conditions.Add("MyUniqueRowId = @rowId");
                                parameters.Add(new SqlParameter() { ParameterName = "@rowId", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = rowId });
                            }
                        }
                        else
                        {
                            conditions.Add("MyUniqueRowId = @rowId");
                            parameters.Add(new SqlParameter() { ParameterName = "@rowId", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = rowId });
                        }
                    }

                    // apply filter conditions
                    if (String.IsNullOrEmpty(filter) == false)
                    {
                        List<string> colConditions = new List<string>();
                        foreach (FormColumn column in form.Columns)
                        {
                            colConditions.Add(column.Id + " like '%' + @val + '%'");
                        }

                        conditions.Add(String.Join(" or ", colConditions.ToArray()));
                        parameters.Add(new SqlParameter() { ParameterName = "@val", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = filter });
                    }

                    // user specific rows filter condition
                    if (form.UserSpecific && rowId != "none")
                    {
                        if (userSpecificRows.Count > 0)
                        {
                            conditions.Add(String.Format("MyUniqueRowId IN ({0})", String.Join(", ", userSpecificRows.ToArray())));
                        }
                        else
                        {
                            conditions.Add("MyUniqueRowId IN ('')");
                        }
                    }

                    // apply all above conditions to SQL query
                    if (conditions.Count > 0)
                    {
                        sql += " where " + String.Join(" and ", conditions.ToArray());
                    }

                    dt = dbAdapter.GetData(sql, parameters);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        int startIndex = pageIndex * pageSize;
                        int endIndex = (pageIndex + 1) * pageSize;
                        if (dt.Rows.Count < endIndex)
                        {
                            endIndex = dt.Rows.Count;
                        }

                        pages = (int)Math.Ceiling(dt.Rows.Count / (double)pageSize);

                        for (int i = startIndex; i < endIndex; i++)
                        {
                            DataRow row = dt.Rows[i];

                            List<string> rowData = new List<string>();
                            foreach (DataColumn column in dt.Columns)
                            {
                                rowData.Add(Convert.ToString(row[column.ColumnName]));
                            }
                            form.Rows.Add(rowData);
                        }
                    }
                }

                return new GetFormDataResult() { Form = form, Pages = pages };
            }

            return null;
        }

        public bool IsRowCommitted(string uniqueName, string rowId)
        {
            string sql = String.Format("select IsCommitted from {0} where MyUniqueRowId = @rowId", ("y" + uniqueName));
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@rowId", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = rowId });

            DataTable dt = dbAdapter.GetData(sql, parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                return (bool)dt.Rows[0]["IsCommitted"];
            }

            return false;
        }

        public void Add(Form form)
        {
            Add(form, false);
        }

        public void Add(Form form, bool clone)
        {
            // 1. create a new entry in Form table
            form.UniqueName = "x" + Guid.NewGuid().ToString().Replace("-", "");
            string sql = "insert into Form values(@description, @uniquename, @userSpecific, @allowCommit)";

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@description", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = form.Description });
            parameters.Add(new SqlParameter() { ParameterName = "@uniquename", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = form.UniqueName });
            parameters.Add(new SqlParameter() { ParameterName = "@userSpecific", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Input, Value = form.UserSpecific });
            parameters.Add(new SqlParameter() { ParameterName = "@allowCommit", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Input, Value = form.AllowCommit });

            dbAdapter.ExecuteSql(sql, parameters);

            // 2. Create a new table with unique name
            List<string> columns = new List<string>();
            foreach (FormColumn column in form.Columns)
            {
                columns.Add(column.Id + " text");
            }
            sql = String.Format("create table {0}(MyUniqueRowId varchar(50), {1})", form.UniqueName, String.Join(",", columns.ToArray()));

            dbAdapter.ExecuteSql(sql);

            // 3. Get form Id and update columns table
            sql = String.Format("select Id from Form where UniqueName = '{0}'", form.UniqueName);
            DataTable dt = dbAdapter.GetData(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                form.Id = (int)dt.Rows[0]["Id"];
                foreach (FormColumn column in form.Columns)
                {
                    if (clone)
                    {
                        column.Id = "c" + Guid.NewGuid().ToString().Replace("-", "");
                    }

                    sql = "insert into FormColumn values(@id, @formId, @description, @columnType, @showInSummary, @options, @helpText, @headerWidth, @displayIndex, @adminEditOnly, @defaultValue)";
                    parameters = new List<SqlParameter>();
                    parameters.Add(new SqlParameter() { ParameterName = "@id", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = column.Id });
                    parameters.Add(new SqlParameter() { ParameterName = "@formId", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = form.Id });
                    parameters.Add(new SqlParameter() { ParameterName = "@description", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = column.Description });
                    parameters.Add(new SqlParameter() { ParameterName = "@columnType", SqlDbType = SqlDbType.SmallInt, Direction = ParameterDirection.Input, Value = (int)column.ColumnType });
                    parameters.Add(new SqlParameter() { ParameterName = "@showInSummary", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Input, Value = column.ShowInSummary });
                    parameters.Add(new SqlParameter() { ParameterName = "@options", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = column.Options });
                    parameters.Add(new SqlParameter() { ParameterName = "@helpText", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = column.HelpText });
                    parameters.Add(new SqlParameter() { ParameterName = "@headerWidth", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = column.HeaderWidth });
                    parameters.Add(new SqlParameter() { ParameterName = "@displayIndex", SqlDbType = SqlDbType.SmallInt, Direction = ParameterDirection.Input, Value = column.DisplayIndex });
                    parameters.Add(new SqlParameter() { ParameterName = "@adminEditOnly", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Input, Value = column.AdminEditOnly });
                    parameters.Add(new SqlParameter() { ParameterName = "@defaultValue", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = column.DefaultValue });

                    dbAdapter.ExecuteSql(sql, parameters);
                }
            }

            // 4. create table which maintains other row properties
            sql = String.Format("create table {0}(MyUniqueRowId varchar(50) not null, UserId int not null, DateTimeAdded datetime not null, DateTimeUpdated datetime not null, IsCommitted bit not null)", ("y" + form.UniqueName));
            dbAdapter.ExecuteSql(sql);
        }

        public void Update(int userId, Form form)
        {
            // 1. update table
            string sql = "update Form set Description = @description, UserSpecific = @userSpecific, AllowCommit = @allowCommit where Id = " + form.Id;
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@description", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = form.Description });
            parameters.Add(new SqlParameter() { ParameterName = "@userSpecific", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Input, Value = form.UserSpecific });
            parameters.Add(new SqlParameter() { ParameterName = "@allowCommit", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Input, Value = form.AllowCommit });

            dbAdapter.ExecuteSql(sql, parameters);

            Form existingForm = GetForm(userId, form.UniqueName);

            sql = "select UniqueName from Form where Id = " + existingForm.Id;
            DataTable dt = dbAdapter.GetData(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                existingForm.UniqueName = (string)dt.Rows[0]["UniqueName"];
            }

            // 2. add new columns and update table holding actual data
            foreach (FormColumn column in form.Columns)
            {
                bool exists = false;
                foreach (FormColumn existingColumn in existingForm.Columns)
                {
                    if (column.Id == existingColumn.Id)
                    {
                        exists = true;
                        break;
                    }
                }

                if (exists)
                {
                    sql = "update FormColumn set Description=@description, ColumnType=@columnType, ShowInSummary=@showInSummary, Options=@options, HelpText=@helpText, HeaderWidth=@headerWidth, DisplayIndex=@displayIndex, AdminEditOnly=@adminEditOnly, DefaultValue=@defaultValue where Id=@id";
                    parameters = new List<SqlParameter>();
                    parameters.Add(new SqlParameter() { ParameterName = "@id", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = column.Id });
                    parameters.Add(new SqlParameter() { ParameterName = "@description", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = column.Description });
                    parameters.Add(new SqlParameter() { ParameterName = "@columnType", SqlDbType = SqlDbType.SmallInt, Direction = ParameterDirection.Input, Value = (int)column.ColumnType });
                    parameters.Add(new SqlParameter() { ParameterName = "@showInSummary", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Input, Value = column.ShowInSummary });
                    parameters.Add(new SqlParameter() { ParameterName = "@options", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = column.Options });
                    parameters.Add(new SqlParameter() { ParameterName = "@helpText", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = column.HelpText });
                    parameters.Add(new SqlParameter() { ParameterName = "@headerWidth", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = column.HeaderWidth });
                    parameters.Add(new SqlParameter() { ParameterName = "@displayIndex", SqlDbType = SqlDbType.SmallInt, Direction = ParameterDirection.Input, Value = column.DisplayIndex });
                    parameters.Add(new SqlParameter() { ParameterName = "@adminEditOnly", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Input, Value = column.AdminEditOnly });
                    parameters.Add(new SqlParameter() { ParameterName = "@defaultValue", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = column.DefaultValue });

                    dbAdapter.ExecuteSql(sql, parameters);
                }
                else
                {
                    sql = "insert into FormColumn values(@id, @formId, @description, @columnType, @showInSummary, @options, @helpText, @headerWidth, @displayIndex, @adminEditOnly, @defaultValue)";
                    parameters = new List<SqlParameter>();
                    parameters.Add(new SqlParameter() { ParameterName = "@id", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = column.Id });
                    parameters.Add(new SqlParameter() { ParameterName = "@formId", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = form.Id });
                    parameters.Add(new SqlParameter() { ParameterName = "@description", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = column.Description });
                    parameters.Add(new SqlParameter() { ParameterName = "@columnType", SqlDbType = SqlDbType.SmallInt, Direction = ParameterDirection.Input, Value = (int)column.ColumnType });
                    parameters.Add(new SqlParameter() { ParameterName = "@showInSummary", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Input, Value = column.ShowInSummary });
                    parameters.Add(new SqlParameter() { ParameterName = "@options", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = column.Options });
                    parameters.Add(new SqlParameter() { ParameterName = "@helpText", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = column.HelpText });
                    parameters.Add(new SqlParameter() { ParameterName = "@headerWidth", SqlDbType = SqlDbType.Decimal, Direction = ParameterDirection.Input, Value = column.HeaderWidth });
                    parameters.Add(new SqlParameter() { ParameterName = "@displayIndex", SqlDbType = SqlDbType.SmallInt, Direction = ParameterDirection.Input, Value = column.DisplayIndex });
                    parameters.Add(new SqlParameter() { ParameterName = "@adminEditOnly", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Input, Value = column.AdminEditOnly });
                    parameters.Add(new SqlParameter() { ParameterName = "@defaultValue", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = column.DefaultValue });

                    dbAdapter.ExecuteSql(sql, parameters);

                    sql = String.Format("alter table {0} add {1} text", existingForm.UniqueName, column.Id);

                    dbAdapter.ExecuteSql(sql);
                }
            }

            // 2. Delete removed columns and update table holding actual data
            foreach (FormColumn existingColumn in existingForm.Columns)
            {
                bool exists = false;
                foreach (FormColumn column in form.Columns)
                {
                    if (column.Id == existingColumn.Id)
                    {
                        exists = true;
                        break;
                    }
                }

                if (exists == false)
                {
                    sql = "delete from FormColumn where Id = @id";

                    parameters = new List<SqlParameter>();
                    parameters.Add(new SqlParameter() { ParameterName = "@id", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = existingColumn.Id });

                    dbAdapter.ExecuteSql(sql, parameters);

                    sql = String.Format("alter table {0} drop column {1}", existingForm.UniqueName, existingColumn.Id);

                    dbAdapter.ExecuteSql(sql);
                }
            }

        }

        public void Remove(int userId, string uniqueName)
        {
            Form form = GetForm(userId, uniqueName);

            // 1. delete from forms list
            string sql = "delete from Form where Id = " + form.Id;
            dbAdapter.ExecuteSql(sql);

            // 2. remove form columns
            sql = "delete from FormColumn where FormId = " + form.Id;
            dbAdapter.ExecuteSql(sql);

            // 3. drop table holding actual form data
            sql = "drop table " + form.UniqueName;
            dbAdapter.ExecuteSql(sql);

            // 4. drop row properties table
            sql = "drop table " + "y" + form.UniqueName;
            dbAdapter.ExecuteSql(sql);
        }

        public Form Clone(int userId, string uniqueName)
        {
            Form form = GetForm(userId, uniqueName);

            Add(form, true);

            return form;
        }

        public Form Copy(int userId, string uniqueName)
        {
            Form form = Clone(userId, uniqueName);

            string sql = String.Format("insert into {0} select * from {1}", form.UniqueName, uniqueName);

            dbAdapter.ExecuteSql(sql);

            return form;
        }

        public FormExportResult Export(int userId, bool isAdmin, string uniqueName)
        {
            ExcelAdapter excelAdapter = new ExcelAdapter(userId, isAdmin, uniqueName);
            excelAdapter.Export();

            return new FormExportResult() { ExportFile = excelAdapter.ExportFileName, FileName = excelAdapter.FileName };
        }

        public GetAttachmentsData GetAttachments(string uniqueName)
        {
            GetAttachmentsData result = new GetAttachmentsData();
            string attachmentsDir = @"C:\FileStore\Attachments\" + uniqueName;
            string formTemplateDir = @"C:\FileStore\FormTemplate\" + uniqueName;
            string rowTemplateDir = @"C:\FileStore\RowTemplate\" + uniqueName;

            // get list of form attachments
            if (Directory.Exists(attachmentsDir))
            {
                string[] files = Directory.GetFiles(attachmentsDir);
                foreach (string file in files)
                {
                    result.FormAttachments.Add(Path.GetFileName(file));
                }
            }

            // get form template
            if (Directory.Exists(formTemplateDir))
            {
                string[] files = Directory.GetFiles(formTemplateDir);
                if (files.Length > 0)
                {
                    result.FormTemplate = Path.GetFileName(files[0]);
                }
            }

            // get row template
            if (Directory.Exists(rowTemplateDir))
            {
                string[] files = Directory.GetFiles(rowTemplateDir);
                if (files.Length > 0)
                {
                    result.RowTemplate = Path.GetFileName(files[0]);
                }
            }

            return result;
        }

        public void UploadFormAttachment(string uniqueName, string fileName, string content)
        {
            string dir = @"C:\FileStore\Attachments\" + uniqueName;
            if (Directory.Exists(dir) == false)
            {
                Directory.CreateDirectory(dir);
            }

            string file = dir + @"\" + fileName;
            File.WriteAllText(file, content);
        }

        public void RemoveFormAttachment(string uniqueName, string fileName)
        {
            string file = @"C:\FileStore\Attachments\" + uniqueName + @"\" + fileName;
            if (File.Exists(file))
            {
                File.Delete(file);
            }
        }

        public void UploadFormTemplate(string uniqueName, string fileName, string content)
        {
            string dir = @"C:\FileStore\FormTemplate\" + uniqueName;
            if (Directory.Exists(dir) == false)
            {
                Directory.CreateDirectory(dir);
            }

            string file = dir + @"\" + fileName;
            File.WriteAllText(file, content);
        }

        public void RemoveFormTemplate(string uniqueName, string fileName)
        {
            string file = @"C:\FileStore\FormTemplate\" + uniqueName + @"\" + fileName;
            if (File.Exists(file))
            {
                File.Delete(file);
            }
        }

        public void UploadRowTemplate(string uniqueName, string fileName, string content)
        {
            string dir = @"C:\FileStore\RowTemplate\" + uniqueName;
            if (Directory.Exists(dir) == false)
            {
                Directory.CreateDirectory(dir);
            }

            string file = dir + @"\" + fileName;
            File.WriteAllText(file, content);
        }

        public void RemoveRowTemplate(string uniqueName, string fileName)
        {
            string file = @"C:\FileStore\RowTemplate\" + uniqueName + @"\" + fileName;
            if (File.Exists(file))
            {
                File.Delete(file);
            }
        }

        public string AddFormData(int userId, bool isCommitted, string uniqueName, List<ColumnValueInput> colValues)
        {
            List<string> columns = new List<string>();
            List<string> values = new List<string>();

            Form form = GetForm(userId, uniqueName);

            string rowId = "'" + Guid.NewGuid().ToString().Replace("-", "") + "'";

            columns.Add("MyUniqueRowId");
            List<SqlParameter> parameters = new List<SqlParameter>();
            values.Add(rowId);
            for (int i = 0; i < colValues.Count; i++)
            {
                ColumnValueInput valueInput = colValues[i];

                string value = valueInput.ColValue;

                FormColumn column = form.Columns[i];
                if (column.ColumnType == ColumnType.File)
                {
                    value = ProcessFileData(uniqueName, value);
                }

                columns.Add(valueInput.ColId);
                values.Add("@col" + i);

                parameters.Add(new SqlParameter() { ParameterName = "@col" + i, SqlDbType = SqlDbType.Text, Direction = ParameterDirection.Input, Value = value });
            }

            string sql = String.Format("insert into {0} ({1}) values ({2})", uniqueName, String.Join(",", columns.ToArray()), String.Join(",", values.ToArray()));
            dbAdapter.ExecuteSql(sql, parameters);

            // insert row properties
            sql = "insert into " + "y" + uniqueName + " values(@rowId, @userId, @dateTimeAdded, @dateTimeUpdated, @isCommitted)";
            parameters.Clear();
            parameters.Add(new SqlParameter() { ParameterName = "@rowId", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = rowId.Trim("'".ToCharArray()) });
            parameters.Add(new SqlParameter() { ParameterName = "@userId", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = userId });
            parameters.Add(new SqlParameter() { ParameterName = "@dateTimeAdded", SqlDbType = SqlDbType.DateTime, Direction = ParameterDirection.Input, Value = DateTime.Now });
            parameters.Add(new SqlParameter() { ParameterName = "@dateTimeUpdated", SqlDbType = SqlDbType.DateTime, Direction = ParameterDirection.Input, Value = DateTime.Now });
            parameters.Add(new SqlParameter() { ParameterName = "@isCommitted", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = isCommitted });

            // TODO: handle edit, admin commit

            dbAdapter.ExecuteSql(sql, parameters);

            return values[0]; // return new row id
        }

        public void UpdateFormData(int userId, bool isCommitted, string uniqueName, string rowId, List<ColumnValueInput> colValues)
        {
            Form form = GetForm(userId, uniqueName);

            List<string> values = new List<string>();
            List<SqlParameter> parameters = new List<SqlParameter>();
            for (int i = 0; i < colValues.Count; i++)
            {
                ColumnValueInput valueInput = colValues[i];

                string value = valueInput.ColValue;

                FormColumn column = form.Columns[i];
                if (column.ColumnType == ColumnType.File)
                {
                    value = ProcessFileData(uniqueName, value);
                }

                values.Add(String.Format("{0} = {1}", valueInput.ColId, "@col" + i));

                parameters.Add(new SqlParameter() { ParameterName = "@col" + i, SqlDbType = SqlDbType.Text, Direction = ParameterDirection.Input, Value = value });
            }

            string sql = String.Format("update {0} set {1} where MyUniqueRowId = '{2}'", uniqueName, String.Join(",", values.ToArray()), rowId);
            dbAdapter.ExecuteSql(sql, parameters);

            // update row properties table
            sql = String.Format("update {0} set DateTimeUpdated = @dateTimeUpdated, IsCommitted = @isCommitted where MyUniqueRowId = @rowId", ("y" + uniqueName));
            parameters.Clear();
            parameters.Add(new SqlParameter() { ParameterName = "@dateTimeUpdated", SqlDbType = SqlDbType.DateTime, Direction = ParameterDirection.Input, Value = DateTime.Now });
            parameters.Add(new SqlParameter() { ParameterName = "@isCommitted", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Input, Value = isCommitted });
            parameters.Add(new SqlParameter() { ParameterName = "@rowId", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = rowId });

            dbAdapter.ExecuteSql(sql, parameters);

            // TODO: handle edit, admin commit
        }

        private string ProcessFileData(string uniqueName, string value)
        {
            if (String.IsNullOrEmpty(value)) return string.Empty;
            if (value.Contains(":") == false) return string.Empty;

            int index = value.IndexOf(':', 0);
            string fileId = value.Substring(0, index);

            int index1 = value.IndexOf(':', index + 1);
            string filename = value.Substring(index + 1, index1 - index - 1);

            int index2 = value.IndexOf(':', index1 + 1);
            string content = value.Substring(index1 + 1, value.Length - index1 - 1);

            string formDir = @"C:\FileStore\FormFiles\" + uniqueName;
            if (Directory.Exists(formDir) == false)
            {
                Directory.CreateDirectory(formDir);
            }

            string path = formDir + @"\" + fileId;
            File.WriteAllText(path, content);

            return fileId + ":" + filename;
        }

        public void DeleteFormData(string uniqueName, string rowId)
        {
            // 1. remove from form's data table
            string sql = String.Format("delete from {0} where MyUniqueRowId = '{1}'", uniqueName, rowId);
            dbAdapter.ExecuteSql(sql);

            // 2. remove from form properties
            sql = String.Format("delete from {0} where MyUniqueRowId = '{1}'", ("y" + uniqueName), rowId);
            dbAdapter.ExecuteSql(sql);
        }

        public void CopyFormData(string uniqueName, string rowId)
        {
            string sql = String.Format("select * from {0} where MyUniqueRowId = '{1}'", uniqueName, rowId);
            DataTable dt = dbAdapter.GetData(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                List<string> cols = new List<string>();
                List<SqlParameter> parameters = new List<SqlParameter>();
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    DataColumn column = dt.Columns[i];
                    string value = (column.ColumnName == "MyUniqueRowId") ? (Guid.NewGuid().ToString().Replace("-", "")) : Convert.ToString(dt.Rows[0][column]);

                    cols.Add("@col" + i);
                    parameters.Add(new SqlParameter() { ParameterName = "@col" + i, SqlDbType = SqlDbType.Text, Direction = ParameterDirection.Input, Value = value });
                }

                sql = String.Format("insert into {0} values({1})", uniqueName, String.Join(",", cols.ToArray()));
                dbAdapter.ExecuteSql(sql, parameters);
            }
        }

        public string CSVExport(int userId, string uniqueName)
        {
            string csv = string.Empty;

            Form form = GetForm(userId, uniqueName);

            string sql = "select * from " + uniqueName;
            DataTable dt = dbAdapter.GetData(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                List<string> columns = new List<string>();
                foreach (DataColumn column in dt.Columns)
                {
                    if (column.ColumnName != "MyUniqueRowId")
                    {
                        foreach (FormColumn col in form.Columns)
                        {
                            if (col.Id == column.ColumnName)
                            {
                                columns.Add(column.ColumnName);

                                csv += col.Description + ",";
                            }
                        }
                    }
                }

                csv = csv.Trim(",".ToCharArray());
                csv += Environment.NewLine;

                foreach (DataRow row in dt.Rows)
                {
                    foreach (string column in columns)
                    {
                        csv += row[column] + ",";
                    }

                    csv = csv.Trim(",".ToCharArray());
                    csv += Environment.NewLine;
                }
            }

            return csv;
        }

        public List<FormAccess> GetFormUsers(string uniqueName)
        {
            List<FormAccess> formUsers = new List<FormAccess>();

            string sql = @"select * from FormAccess where UniqueName = @id";

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@id", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = uniqueName });

            DataTable dt = dbAdapter.GetData(sql, parameters);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    FormAccess access = new FormAccess();
                    access.UserId = (int)row["UserId"];
                    access.ViewAccess = (bool)row["ViewAccess"];
                    access.EditAccess = (bool)row["EditAccess"];
                    access.AdminAccess = (bool)row["AdminAccess"];

                    formUsers.Add(access);
                }
            }

            return formUsers;
        }

        public void SetFormUsers(string uniqueName, FormAccess[] formUsers)
        {
            string sql = @"delete from FormAccess where UniqueName = @id";

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter() { ParameterName = "@id", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = uniqueName });

            dbAdapter.ExecuteSql(sql, parameters);

            if (formUsers.Length > 0)
            {
                foreach (FormAccess access in formUsers)
                {
                    sql = @"insert into FormAccess values(@uniqueName, @userId, @viewAccess, @editAccess, @adminAccess)";

                    parameters.Clear();
                    parameters.Add(new SqlParameter() { ParameterName = "@uniqueName", SqlDbType = SqlDbType.VarChar, Direction = ParameterDirection.Input, Value = uniqueName });
                    parameters.Add(new SqlParameter() { ParameterName = "@userId", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = access.UserId });
                    parameters.Add(new SqlParameter() { ParameterName = "@viewAccess", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Input, Value = access.ViewAccess });
                    parameters.Add(new SqlParameter() { ParameterName = "@editAccess", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Input, Value = access.EditAccess });
                    parameters.Add(new SqlParameter() { ParameterName = "@adminAccess", SqlDbType = SqlDbType.Bit, Direction = ParameterDirection.Input, Value = access.AdminAccess });

                    dbAdapter.ExecuteSql(sql, parameters);
                }
            }
        }

        public List<FormTemplate> GetFormTemplates()
        {
            List<FormTemplate> templates = new List<FormTemplate>();

            string sql = "select * from FormTemplate order by Category";
            DataTable dt = dbAdapter.GetData(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    FormTemplate template = new FormTemplate();
                    template.FormId = (string)row["FormId"];
                    template.Description = (string)row["Description"];
                    template.Category = (string)row["Category"];

                    templates.Add(template);
                }
            }

            return templates;
        }
    }
}