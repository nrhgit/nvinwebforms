﻿var serviceurl = "http://localhost:57275/AppService.asmx/";
var result = {};

var index = 1

function serviceCall(method, d, useToken, readToken) {
    if(readToken === undefined){
        var tkn = readCookie('token');
        if (typeof useToken !== undefined && tkn == "") {
            window.location.href = "/_auth/lgn.html";
        }
    }

    var dt = { data: JSON.stringify(d) };
    if (typeof useToken !== undefined) {
        dt = { token: tkn, data: JSON.stringify(d) };
    }

    return $.when(
        $.ajax({
            type: "POST",
            data: dt,
            cache: false,
            dataType: "json",
            url: serviceurl + method,
        }).done(function (res, status) {
            result = res;
        }).fail(function (msg, status) {
            alert("ERROR: " + JSON.stringify(msg));
        })
    );
}

function download(method, d) {
    var tkn = readCookie('token');
    tkn = tkn.replace(/\+/g, '^');
    window.location.href = serviceurl + method + "?token="+tkn+"&data=" + d;
}

function exportFormData(formId) {
    var tkn = readCookie('token');
    tkn = tkn.replace(/\+/g, '^');
    window.location.href = serviceurl + "ExportForm?token=" + tkn + "&form=" + formId;
}

function downloadFormFile(formId, fileId, fileName) {
    window.location.href = serviceurl + "DownloadFormFile?form=" + formId + "&file=" + fileId + "&name=" + fileName;
}

function downloadAttachmentFile(formId, fileName) {
    window.location.href = serviceurl + "DownloadAttachment?form=" + formId + "&name=" + fileName;
}

function setCookie(key, value) {
    var expires = new Date();
    expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + ';expires=' + expires.toUTCString() + "; path=/";
}

function readCookie(name) {
    var cookies = document.cookie;
    var cookieArr = cookies.split(";");
    for (i = 0; i < cookieArr.length; i++) {
        var value = cookieArr[i];
        var vals = value.split("=");
        if (vals[0].trim() == name) {
            return vals[1];
        }
    }
    return "";
}