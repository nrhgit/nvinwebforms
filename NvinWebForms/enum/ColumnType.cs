﻿using System;
using System.Collections.Generic;
using System.Web;

namespace NvinWebForms
{
    public enum ColumnType
    {
        Numeric,
        Text,
        LongText,
        Bool,
        Options,
        OptionsMultiSelect,
        File,
        Date
    }
}